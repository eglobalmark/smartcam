import threading
from base64 import b64encode
import serial

from lorasender_pb2 import *


class LoraReportSender(threading.Thread):
    def __init__(self, device_path, report_queue):
        threading.Thread.__init__(self)
        self.device_path = device_path
        self.report_queue = report_queue
        self.serial_port = serial.Serial(device_path,9600)

    def run(self):
        while True:
            report = self.report_queue.get()
            request_lora = request()
            payload = report.to_lora_packet()
            # TODO : defensive check / payload size
            request_lora.command = request.command_type.SEND
            request_lora.data = payload
            buffer = request_lora.SerializeToString()
            encoded_buffer = b64encode(buffer)
            self.send_to_modem(encoded_buffer)

    def send_to_modem(self, b64_encoded_data):
        print("Sending request_to_modem : %s" % b64_encoded_data)
        self.serial_port.write(b64_encoded_data)
        self.serial_port.write(b"\n")        