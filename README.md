# SmartCam

Smart camera application for the Fed4IoT project. It monitors a folder 
where the camera is uploading periodically some images. When an image 
upload is detected, the application processes it and in case of a positive 
detection, send an alert using the lorasender modem.


