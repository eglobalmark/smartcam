from queue import Queue, Empty
import sys
import time
import os
import threading

import pyinotify

from analysis import mobilenetv2_image_analyzer
#from analysis import dummy_file_analysis

from LoRa import LoraReportSender


class ImageFile:
    def __init__(self, file_path):
        self.file_path = file_path
        self.is_upload_finished = False


class ImageFileProcessor(threading.Thread):
    def __init__(self, input_queue, cb_process, report_queue):
        threading.Thread.__init__(self)
        self.input_queue = input_queue
        self.cb_process = cb_process
        self.report_queue = report_queue

    def run(self):
        print("ImageFileProcessor:Running")
        while True:
            print("ImageFileProcessor:Ready")
            file_to_process = self.input_queue.get()   # blocking call
            print("ImageFileProcessor:Processing:%s" % file_to_process.file_path)
            report = self.cb_process(file_to_process)
            os.remove(file_to_process.file_path)
            if report:
                self.report_queue.put(report)


class ImagesEventHandler(pyinotify.ProcessEvent):
    IMAGES_REGEX = [r".*\.jpg$"]

    def __init__(self, processing_queue):
        super().__init__()
        self.processing_queue = processing_queue

    def process_IN_CREATE(self, event):
        print("ImagesEventHandler:New:%s" % event.pathname)

    def process_IN_CLOSE_WRITE(self, event):
        # When a new file is detected, push it into the processing queue
        print("ImagesEventHandler:Uploaded:%s" % event.pathname)
        if (os.path.splitext(event.pathname)[1] in ['.jpg','.JPG']):
            self.processing_queue.put(ImageFile(event.pathname))


class ThreadsManager:
    def __init__(self, watch_path):
        self.watch_path = watch_path

        self.uploaded_files = Queue()
        self.reports = Queue()

        self.detector = mobilenetv2_image_analyzer()

        print("Monitoring folder : %s" % watch_path)
        self.notify_mask = pyinotify.IN_CLOSE_WRITE | pyinotify.IN_CREATE
        self.new_file_manager = pyinotify.WatchManager()
        self.new_file_handler = ImagesEventHandler(self.uploaded_files)
        self.new_file_notifier = pyinotify.Notifier(self.new_file_manager, self.new_file_handler)
        self.new_file_watch = self.new_file_manager.add_watch(self.watch_path, self.notify_mask)

        #self.file_processor = ImageFileProcessor(self.uploaded_files, dummy_file_analysis, self.reports)
        self.file_processor = ImageFileProcessor(self.uploaded_files, self.detector.analyze, self.reports)

        self.report_sender = LoraReportSender("/dev/ttyACM0", self.reports)

    def run_loop(self):
        self.file_processor.start()
        self.report_sender.start()
        try:
            self.new_file_notifier.loop()
        except KeyboardInterrupt:
            print("Keyboard interrupt")


if __name__ == "__main__":
    src_path = sys.argv[1] if len(sys.argv) > 1 else '.'
    tm = ThreadsManager(src_path)
    tm.run_loop()
