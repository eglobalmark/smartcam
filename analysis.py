from datetime import datetime
import os
from collections import defaultdict

from google.protobuf.timestamp_pb2 import Timestamp
import camdetection_pb2

import jetson.inference
import jetson.utils


def dummy_file_analysis(image_file):
    with open(image_file.file_path):
        print("image file is open")
    report = ImageAnalysisReport(image_file)
    report.cam_id = 1
    report.time_shot = datetime.now()
    report.detected['size'] = os.path.getsize(image_file.file_path)
    return report


class mobilenetv2_image_analyzer :
    def __init__(self):
        self.net = jetson.inference.detectNet("ssd-mobilenet-v2", [], 0.5)
        #print("Detectnet is initialized\nKnown classes:%s"%"\n".join(["%d:%s"%(icl,self.net.GetClassDesc(icl)) for icl in range(self.net.GetNumClasses())]  ))

    def analyze(self,image_file):
        print("mobilenetv2_image_analyzer:analyze:%s"%image_file.file_path)
        img, width, height = jetson.utils.loadImageRGBA(image_file.file_path)
        detections = self.net.Detect(img, width, height, 'box,labels,conf')
        if len(detections)==0 :
            print("mobilenetv2_image_analyzer:Nothing detected")
        for detection in detections:
            detclass = self.net.GetClassDesc(detection.ClassID)
            print(detclass)
            print(detection)
            report = ImageAnalysisReport(image_file)
            if (detclass=='person'): report.detection.nb_person += 1
            elif (detclass=='car'): report.detection.nb_car += 1
            return report


class ImageAnalysisReport:

    def __init__(self, image_file):
        self.image_file = image_file
        self.time_shot = None
        self.time_processed = Timestamp()
        self.time_processed.GetCurrentTime()
        self.detection = camdetection_pb2.camdetection()

    def to_lora_packet(self):
        self.detection.idcam = 1
        self.detection.timestamp = self.time_processed.seconds
        return self.detection.SerializeToString()
